#ifndef ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H

#define TESTS_COUNT 5
#include <stdbool.h>

bool test1();

bool test2();

bool test3();

bool test4();

bool test5();

#endif //ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H
