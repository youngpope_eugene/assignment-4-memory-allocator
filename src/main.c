#include "tests.h"
#include <stdio.h>


int main(void){
    int success_test = 0;
    success_test += test1();
    success_test += test2();
    success_test += test3();
    success_test += test4();
    success_test += test5();

    printf("Прошло %d из %d тестов\n", success_test, TESTS_COUNT);
    return 0;
}