#include "mem.h"
#include "mem_internals.h"
#include "tests.h"
#include <stdio.h>

bool test1(){
    printf("Тест 1, Обычное выделение памяти\n");
    void *heap = heap_init(REGION_MIN_SIZE);
    void *malloc = _malloc(239);
    debug_heap(stdout, heap);
    if (!malloc){
        fprintf(stderr, "%s", "FAIL");
        return false;
    }
    _free(malloc);
    munmap(heap, size_from_capacity((block_capacity){.bytes=REGION_MIN_SIZE}).bytes);
    printf("Тест 1, Успешно\n");
    return true;
}

bool test2(){
    printf("Тест 2, Освобождение одного блока из нескольких выделенных\n");
    void *heap = heap_init(REGION_MIN_SIZE);
    void *malloc1 = _malloc(239);
    void *malloc2 = _malloc(239);
    debug_heap(stdout, heap);
    _free(malloc1);
    debug_heap(stdout, heap);
    _free(malloc2);
    munmap(heap, size_from_capacity((block_capacity){.bytes=REGION_MIN_SIZE}).bytes);
    printf("Тест 2, Успешно\n");
    return true;
}

bool test3(){
    printf("Тест 3, Освобождение двух блоков из нескольких выделенных\n");
    void *heap = heap_init(REGION_MIN_SIZE);
    void *malloc1 = _malloc(239);
    void *malloc2 = _malloc(239);
    void *malloc3 = _malloc(239);
    debug_heap(stdout, heap);
    _free(malloc1);
    _free(malloc2);
    debug_heap(stdout, heap);
    _free(malloc3);
    munmap(heap, size_from_capacity((block_capacity){.bytes=REGION_MIN_SIZE}).bytes);
    printf("Тест 3, Успешно\n");
    return true;
}

bool test4(){
    printf("Тест 4, Память закончилась, новый регион памяти расширяет старый\n");
    void *heap = heap_init(REGION_MIN_SIZE);
    void *malloc = _malloc((REGION_MIN_SIZE + 1));
    debug_heap(stdout, heap);
    _free(malloc);
    munmap(heap, size_from_capacity((block_capacity){.bytes=REGION_MIN_SIZE+1}).bytes);
    printf("Тест 4, Успешно\n");
    return true;
}

bool test5(){
    printf("Тест 5, Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте\n");
    void *heap = heap_init(REGION_MIN_SIZE);
    struct block_header *bh = (struct block_header *) heap;
    void *malloc1 = _malloc(bh->capacity.bytes);
    debug_heap(stdout, heap);
    void *malloc11 = mmap(bh->contents + bh->capacity.bytes, REGION_MIN_SIZE*2, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0);
    void *malloc2 = _malloc(239);
    debug_heap(stdout, heap);
    _free(malloc1);
    _free(malloc2);
    munmap(malloc11, size_from_capacity((block_capacity) {.bytes=5000}).bytes);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=REGION_MIN_SIZE}).bytes);
    printf("Тест 5, Успешно\n");
    return true;
}